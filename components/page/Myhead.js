import React from 'react'
import { Link } from 'react-router-dom'
import '../../css/App.css'


class Myhead extends React.Component {
    // constructor(){
    //      super()
    // }

    componentDidMount() {
        if(window.location.pathname=='/tj'){
                this.refs.a.style.borderBottom = "3px solid orange"
        }else if(window.location.pathname=='/hot'){
            this.refs.b.style.borderBottom = "3px solid orange"
        }else if(window.location.pathname=='/search'){
            this.refs.c.style.borderBottom = "3px solid orange"
        }
    }
    fn(ind) {
        let x = this.refs
        for (let key in x) {
            this.refs[key].style.borderBottom = "3px solid #fff"
        }
        this.refs[ind].style.borderBottom = "3px solid orange"
    }
    render() {
        return (
            <header>

                <ul className="layout">
                    <li >
                        <Link to="/tj" ref="a" onClick={this.fn.bind(this, 'a')}>推荐</Link>
                    </li>
                    <li >
                        <Link to="/hot" ref="b" onClick={this.fn.bind(this, 'b')}>热歌榜</Link>
                    </li>
                    <li >
                        <Link to="/search" ref="c" onClick={this.fn.bind(this, 'c')}>搜索</Link>
                    </li>
                </ul>
            </header>
        )
    }
}
export default Myhead;