import React from 'react'
import {Switch, Route,Redirect} from 'react-router-dom'
import Myhead from './page/Myhead'


import '../css/reset.css'
import '../css/App.css'

class Index extends React.Component {
    render() {
        // console.log(this.props)
        return (
            <div>
                <div className="box layout">
                    <p>优音乐</p>
                    <p>下载APP</p>
                </div>
                <Myhead></Myhead>
          
                <Switch>
                    {/* <Route path="/index/tj" component={Mytj}></Route>
                    <Route path="/index/hot" component={Hot}></Route>
                    <Route path="/index/search" component={Search}></Route> */}
                        {
                      this.props.children.map((item,index)=>{
                          if(item.path==="*"){
                            return (<Redirect key={index} path={item.path} to={item.redirect}></Redirect>)
                          }else{
                            return (<Route key={index} path={item.path} component={item.component}></Route>)
                          }
                      })
                    }
                </Switch>
               

            </div>



        )
    }
}
export default Index