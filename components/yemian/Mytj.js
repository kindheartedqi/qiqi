import React from 'react'
import AwesomeSwiper from 'react-awesome-swiper';
import axios from 'axios';
import {withRouter } from 'react-router-dom'
import '../../css/Mytj.css'

//this config is same as idangrous swiper
const config = {
    loop: true,
    autoplay: {
        delay: 3000,
        stopOnLastSlide: false,
        disableOnInteraction: true,
    },
    // Disable preloading of all images
    preloadImages: false,
    // Enable lazy loading
    lazy: true,
    speed: 500,
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
    pagination: {
        el: '.swiper-pagination',
        bulletElement: 'li',
        hideOnClick: true,
        clickable: true,
    },
    on: {
        slideChange: function () {
            // console.log(this.activeIndex);
        },
    },
};

class Mytj extends React.Component {
    swiperRef = null
    constructor() {
        super()
        this.state = {
            bannerarr: [],
            recommendarr: [],
            newMusicarr: []
        }
    }
    goNext = () => {//use `this.swiperRef.swiper` to get the instance of Swiper
        this.swiperRef.swiper.slideNext();
    }
    recommend() {
        return axios.get(this.$api.personalized)
    }
    newMusicarr() {
        return axios.get(this.$api.newSong)
    }
    datals(ids){
        this.props.history.push('/songlist/'+ids)
    }
    componentDidMount() {
        this.$axios.get(this.$api.banner)
            .then(res => {
                // console.log(res.data,"res")
                this.setState({
                    bannerarr: res.data.banners
                })
            });
        axios.all([this.recommend(), this.newMusicarr()])
            .then(axios.spread((recommendarr, newMusicarr) => {
                // console.log(recommendarr)
                this.setState({
                    recommendarr: recommendarr.data.result,
                    newMusicarr: newMusicarr.data.result,
                })

            }))
    }
    render() {
        return (
            <div className="tuijian clearfix">
                <h2>推荐歌单</h2>
                <div className=" music">
                    {/* 轮播图 */}
                    <AwesomeSwiper ref={ref => (this.swiperRef = ref)} config={config} className="your-classname banner">
                        <div className="swiper-wrapper">
                            {/* <div className="swiper-slide">slider1</div>
                        <div className="swiper-slide">slider2</div>
                        <div className="swiper-slide">slider3</div> */}
                            {
                                this.state.bannerarr.map((item, index) => {
                                    return <div className="swiper-slide" key={index}>
                                        <img src={item.imageUrl} />
                                    </div>
                                })
                            }
                        </div>
                        {/* <!--左箭头--> */}
                        <div className="swiper-button-prev"></div>
                        {/* <!--右箭头--> */}
                        <div className="swiper-button-next"></div>
                        <div className="swiper-pagination"></div>
                    </AwesomeSwiper>
                    <div className="pics clearfix">

                        {
                            this.state.recommendarr.map((item, index) => {
                                return (
                                    <div className="pic" key={index} onClick={this.datals.bind(this,item.id)}>
                                        <img src={item.picUrl} alt="" />
                                        <p>{item.name}</p>
                                    </div>
                                )
                            })
                        }

                    </div>
                </div>
                <div className="newMusic">
                    <h2>最新音乐</h2>
                    {
                        this.state.newMusicarr.map((item, index) => {
                            return (
                                <div className="word" key={item.id}>
                                    <div className="left">
                                        <p>
                                            {item.name}
                                        </p>
                                        <i>SQ</i>
                                        {
                                            item.song.artists.map((val,index) => {
                                                let sep=item.song.artists.length>1 && index !=item.song.artists.length-1?<span>/</span>:""
                                                return (
                                                    <span key={index}>
                                                        {val.name}{sep}
                                                    </span>
                                                )
                                            })
                                        }

                                  
                                    </div>
                                    <div className="right">
                                        <img src="https://ss0.bdstatic.com/70cFuHSh_Q1YnxGkpoWK1HF6hhy/it/u=2792802744,99354550&fm=26&gp=0.jpg" alt="" />
                                    </div>
                                </div>
                            )

                        })
                    }
                </div>

            </div>
        )
    }
}
export default withRouter(Mytj)