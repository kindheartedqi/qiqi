import React from 'react'
import axios from 'axios';
import '../../css/Hot.css'
class Hot extends React.Component {
    constructor() {
        super()
        this.state = {
            updateTime: "",   //热歌榜日期  updateTime
            coverImg: "",    //最上面的图片
            name: '',
            songs: []
        }
    }
    toplist() {
        return axios.get(this.$api.toplist)
    }
    detals() {

    }
    componentDidMount() {
        console.log(this.props.match.params)
        let url = this.props.match.params.id ? this.$api.songList + '?id=' + this.props.match.params.id : this.$api.toplist;
        this.$axios.get(url)
            .then(res => {
                console.log(res.data)
                let date = new Date(res.data.playlist.updateTime);
                let month = date.getMonth() + 1 > 9 ? date.getMonth() + 1 : '0' + date.getMonth() + 1
                let day = date.getDate() > 9 ? date.getDate() : '0' + date.getDate();
                this.setState({
                    coverImg: res.data.playlist.coverImgUrl,
                    name: res.data.playlist.name,
                    songs: res.data.playlist.tracks,
                    updateTime: `${month}月${day}日`

                })
            })
    }
    render() {
        let picitem;
        let backbtn;
        if (this.props.match.params.id) {
            picitem = (
                <div className="banner">
                    <img src={this.state.coverImg} />
                </div>
            )
        } else {
            backbtn = (<div className="banner">
                <img src={this.state.coverImg} />
            </div>)
        }
        return (
            <div>
                <div>
                    <div className="mask" style={{ background: 'url(' + this.state.coverImg + ')' }}>
                        {picitem}

                    </div>

                    {backbtn}
                </div>
                <div className="child">
                    {
                        this.state.songs.map((item, index) => {
                            return (
                                <div className="word" key={item.id}>
                                    <div className="num">
                                     {
                                         index+1>9?index+1:'0'+(index+1)
                                     }
                                    </div>
                                    <div className="left">
                                        <p>
                                            {item.name}
                                        </p>
                                        <i>SQ</i><span>
                                            {
                                                 item.ar.map((val,index) => {
                                                    let sep=item.ar.length>1 && index !=item.ar.length-1?<span>/</span>:""
                                                    return (
                                                        <span key={index}>
                                                            {val.name}{sep}
                                                        </span>
                                                    )
                                                })
                                            }
                                        </span>
                                    </div>
                                    <div className="right">
                                        <img src="https://ss0.bdstatic.com/70cFuHSh_Q1YnxGkpoWK1HF6hhy/it/u=2792802744,99354550&fm=26&gp=0.jpg" alt="" />
                                    </div>
                                </div>
                            )
                        })
                    }

                </div>
            </div>
        )
    }
}
export default Hot