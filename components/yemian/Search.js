import React from 'react'
import '../../css/Search.css'
class Search extends React.Component {
    constructor() {
        super()
        this.state = {
            name: "",
            songarr: [],
            searchhotarr: []
        }

    }
    fn(e) {
        this.setState({
            name: e.target.value
        })
    }
    fn1(e) {
        if (e.keyCode === 13) {
            if(this.state.name){
            this.$axios.get(this.$api.search + '?keywords=' + this.state.name)
                .then(res => {
                    // console.log(res)
                    this.setState({
                        songarr: res.data.result.songs,
                        searchhotarr:[]
                    })
                    // console.log(this.state.songarr)
                })
            }else{
                this.setState({
                    songarr:[],
                    
                })
                this.$axios.get(this.$api.searchhot)
                .then(res => {
                    // console.log(res)
                    this.setState({
                        searchhotarr: res.data.result.hots
                    })
                    // console.log(this.state.searchhotarr)
                })
                
            }
        }
    }
    change(num){
        // console.log(this.state.searchhotarr[num])
        this.setState({
            name:this.state.searchhotarr[num].first
        })
      
    }
    componentDidMount() {
        this.$axios.get(this.$api.searchhot)
            .then(res => {
                // console.log(res)
                this.setState({
                    searchhotarr: res.data.result.hots
                })
                // console.log(this.state.searchhotarr)
            })
    }
    render() {
        return (
            <div>
                <div className="box layout">
                    <input value={this.state.name} onKeyUp={this.fn1.bind(this)} onChange={this.fn.bind(this)} type="text" placeholder="请输入关键词" />
                </div>
                <dl>
                    {
                        this.state.songarr.map(item => {
                            return (
                                <li key={item.id} >{item.name}</li>
                            )
                        })
                    }
                </dl>
                {
                    this.state.searchhotarr.map((item,index) => {
                        return (
                            <div className="radius layout" key={index} onClick={this.change.bind(this,index)}>
                                {item.first}
                            </div>
                        )

                    })
                }


            </div>
        )
    }
}
export default Search