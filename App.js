import React from 'react';
// import Index from './components/index'
// import Login from './components/Login'
// import Register from './components/Register'
import router from './route'
import { BrowserRouter, Switch, Route,Redirect } from 'react-router-dom'


class App extends React.Component {
  renderComponent(com,p){
    // console.log(com)
    // if(com.needlogin){
      //    判断有没有登录信息
          // if(localStorage.getItem('userinfo')){
          //     return <com.component/>
          // }else{
          //     return <Redirect to="/login"/>
          // }
      // }else{
        return <com.component {...p}  children={com.children} />
      // }
}
  render() {
    return (
      <BrowserRouter>
        <Switch>

          {/* <Route path="/login" component={Login}></Route>
          <Route path="/register" component={Register}></Route>
          <Route path="/index" component={Index}></Route> */}
          {
                   router.map((route,key)=>{
                    return  <Route key={key} path={route.path} render={(props)=>(this.renderComponent(route,props))}></Route>
                })
          }
          {/* <Route path="*">
            <Redirect to="/index/tj"></Redirect>
          </Route> */}

        </Switch>
      </BrowserRouter>

    )

  }

}
export default App;
